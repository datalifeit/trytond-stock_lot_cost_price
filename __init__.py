# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import stock


def register():
    Pool.register(
        stock.Lot,
        stock.LotsByLocations,
        module='stock_lot_cost_price', type_='model')
